// ==UserScript==
// @name       Livestreamer bridge
// @namespace  https://bitbucket.org/majosa/livestreamer-wrapper-for-vlc
// @version    0.1
// @description  Substitute shitty flash objects by simplistic and efficient VLC objects
// @copyright  2014 majosa
// @include *
// @exclude yoba.vg
// ==/UserScript==
substitutionTable=[[{"localName":"object","data":"http://(?:(?:\\w*\\.)twitch\\.tv|(?:\\w*\\.)justin\\.tv)/widgets/live_embed_player.swf\\?channel=(\\w*)"},{"type":"application/x-vlc-plugin","target":"livestreamer://twitch.tv/{0}"}],
                   [{"localName":"object","data":"http://www-cdn\\.jtvnw\\.net/swflibs/TwitchPlayer\\.swf","flashvars":".*channel=(\\w*).*"},{"type":"application/x-vlc-plugin","target":"livestreamer://twitch.tv/{0}"}],
                   [{"localName":"object","data":"http://cybergame\\.tv/players/cyberplayer\\d{0,2}\\.swf\\?channel=(\\w*)"},{"type":"application/x-vlc-plugin","target":"livestreamer://cybergame.tv/{0}"}],
                   [{"localName":"embed","src":"http://cybergame\\.tv/players/cyberplayer\\d{0,2}\\.swf\\?channel=(\\w*).*"},{"type":"application/x-vlc-plugin","target":"livestreamer://cybergame.tv/{0}"}],
                   [{"localName":"object","data":"http://static-cdn1\\.ustream\\.tv/swf/live/viewer:281\\.swf.*","flashvars":".*cid=(\\d*).*"},{"type":"application/x-vlc-plugin","target":"livestreamer://ustream.tv/channel/{0}"}],
                   [{"localName":"object","data":"http://(?:\\w*\\.)ustream\\.tv/flash/viewer\\.swf.*","flashvars":".*cid=(\\d*).*"},{"type":"application/x-vlc-plugin","target":"livestreamer://ustream.tv/channel/{0}"}]]

if (!String.prototype.format) {
  String.prototype.format = function(args) {
    return this.replace(/{(\d+)}/g, function(match, number) { 
      return typeof args[number] != 'undefined'
        ? args[number]
        : match
      ;
    });
  };
}

function copyProps(source,dest){
    for(var i in source){
        try{
                dest[i]=source[i];
        }catch( ex){
            
        }
    }
}
function onObservation(event){
    event.forEach(function(mutation){
        if(mutation.type=="attributes"){
            if(mutation.target.getAttribute("old"+mutation.attributeName)!=null && mutation.target.getAttribute("old"+mutation.attributeName)!=mutation.target.getAttribute(mutation.attributeName)) attemptMatchElement(mutation.target);
        }else if(mutation.type=="subtree" || mutation.type=="childList"){
            for(var z=0;z<mutation.addedNodes.length;z++){
                var element=mutation.addedNodes[z];
                var rs=document.evaluate("//object|//embed|//video",element,null,XPathResult.UNORDERED_NODE_SNAPSHOT_TYPE,null);
                if(rs){
                    for(var i=0;i<rs.snapshotLength;i++){
                        attemptMatchElement(rs.snapshotItem(i));
                    }
                }
                //attemptMatchElement(node);
            }
        }
    });
}

var observer=new MutationObserver(onObservation);
observer.observe(document.body,{"childList": true,"subtree":true});

//setInterval(shittyWatcherBecauseATPDoesntWantsUseSetAttribute,3000);

function attemptMatchElement(element){
    if(!element) return;
    if(!element.getAttribute("observed")){
        observer.observe(element,{"attributes":true});
        element.setAttribute("observed",true);
    }
    for(var z=0;z<substitutionTable.length;z++)
    {
        var result=[];
        var matchFailed=false;
        for(var x in substitutionTable[z][0])
        {
            if(element.getAttribute(x)!=null){
                var presult=element.getAttribute(x).match(substitutionTable[z][0][x]);
                element.setAttribute("old"+x,element.getAttribute(x));
                if(presult){
                    presult.shift();
                    result=result.concat(presult);
                }else{
                    matchFailed=true;  
                }
            }else if(x in element){
                var presult=element[x].match(substitutionTable[z][0][x]);
                element.setAttribute("old"+x,element[x]);
                if(presult){
                    presult.shift();
                    result=result.concat(presult);
                }else{
                    matchFailed=true;  
                }
            }else{
                var rs=document.evaluate("//param[@name='"+x+"']",element,null,XPathResult.FIRST_ORDERED_NODE_TYPE,null);
                if(rs.singleNodeValue){
                    var presult=rs.singleNodeValue.value.match(substitutionTable[z][0][x]);

                    if(presult){
                        presult.shift();
                        result=result.concat(presult);
                    }else{
                        matchFailed=true;  
                    }
                    //observer.observe(rs.singleNodeValue,{"attribute":true});
                }else{
                    matchFailed=true;
                }
            }
        }
        if(result.length>0 && !matchFailed)
        {
            var newNode;
            if("localName" in substitutionTable[z][1] && substitutionTable[z][1]["localName"]!=element["localName"]){
                GM_log("copy init");
                newNode=document.createElement(substitutionTable[z][1]["localName"]);
                copyProps(element,newNode);
                element.parentElement.replaceChild(newNode,element);
            }else{
                newNode=element;
            }
            for(var x in substitutionTable[z][1])
            {
                newNode.setAttribute("old"+x,newNode.getAttribute(x));
                newNode.setAttribute(x,substitutionTable[z][1][x].format(result));
            }
            //watchedElements.push(newNode);
            GM_log("newNode");
            GM_log(newNode.playlist);
            newNode.playlist.clear();
            newNode.playlist.add(newNode.getAttribute("target"));
            newNode.playlist.playItem(1);
            //newNode.addEventListener("DOMAttrModified",onAttrModified,false);
        }
    }
}

var possibleVictims=document.getElementsByTagName("object");
//possibleVictims=possibleVictims.concat(document.getElementsByTagName("embed"));
for(var i=0;i<possibleVictims.length;i++)
{
    attemptMatchElement(possibleVictims[i]);
}

possibleVictims=document.getElementsByTagName("embed");
//possibleVictims=possibleVictims.concat(document.getElementsByTagName("embed"));
for(var i=0;i<possibleVictims.length;i++)
{
    attemptMatchElement(possibleVictims[i]);
}
