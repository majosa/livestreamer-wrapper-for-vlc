/**
 * @file livestreamer.c
 * @brief Bridge to livestreamer application parser
 */
#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#if defined (__unix__) || (defined (__APPLE__) && defined (__MACH__))
#define __posix__
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
/* VLC core API headers */
#include <vlc_common.h>
#include <vlc_access.h>

#include <vlc_input.h>
#include <vlc_plugin.h>

#ifdef _WIN32
#include <windows.h>
#include <io.h>
#include <fcntl.h>
#elif defined (__posix__)
#include <sys/types.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <unistd.h>
#include <signal.h>
#endif



#define MODULE_STRING "liblivestreamer"
#define DOMAIN  "vlc-livestreamer"
#define _(str)  dgettext(DOMAIN, str)
#define N_(str) (str)

static int Open(vlc_object_t *);
static void Close(vlc_object_t *);
static int Control(access_t *p_access, int i_query, va_list args);

/* Module descriptor */
vlc_module_begin()
    set_shortname(N_("Livestreamer"))
    set_description(N_("Bridge to livestreamer parsing"))
    set_capability("access", 60)
    add_shortcut("livestreamer");
    set_callbacks(Open, Close)
    set_category(CAT_INPUT)
    set_subcategory( SUBCAT_INPUT_ACCESS )
vlc_module_end ()

#if defined (__posix__)
#define LIVESTREAMER_PATH "/usr/bin/livestreamer"
#elif defined (_WIN32)
#define LIVESTREAMER_PATH "\"C:\\Program Files\\livestreamer\\livestreamer.exe\""

typedef struct{
  HANDLE entry;
  HANDLE exit;
  HANDLE pipe;
}PipeCombo;

typedef struct{
  PipeCombo* output;
  PipeCombo* input;
  PipeCombo* error;
  PROCESS_INFORMATION pi;
}Process;

#endif

typedef struct{
#if defined (__posix__)
  pid_t livestreamerProcess;
  FILE* livestreamerStdout;
#elif defined (_WIN32)
  Process* livestreamerProcess;
  OVERLAPPED outputExitTransaction;
  unsigned char* buffer;
#endif
}ModuleInstance;

/* Forward declarations */
static void ErrMessage(access_t* p_access, const char* msg,unsigned long errCode);
char* RandomString(unsigned int n);
#if defined (__posix__)
static pid_t popen2(const char **command, int *infp, int *outfp, int *errfp);
static ssize_t Read(access_t* p_access,uint8_t* size,size_t sz);
#elif defined (_WIN32)
PipeCombo* CreatePipeCombo(access_t* p_access,char flags);
void FreePipeCombo(PipeCombo* combo);
Process* InvokeCommand(access_t* p_access,char* command,unsigned char asyncPipe);
void FreeProcess(Process* process);
static block_t* Block(access_t* p_access);
#endif

char* RandomString(unsigned int n){
    char* r=malloc(n+1);
    unsigned int i;
    for(i=0;i<n;i++){
      r[i]='A' + (rand() % 26);
    }
    r[i]='\0';
    return r;
}

void ErrMessage(access_t* p_access, const char* msg,unsigned long errCode){
      char* lst=malloc(sizeof("Error code:xxxxx ")+strlen(msg)+1);
      sprintf(lst,"Error code:%lu ",errCode);
      strcat(lst,msg);
      msg_Err(p_access,lst);
      free(lst);
}

#ifdef __posix__

#define READ 0
#define WRITE 1


pid_t popen2(const char **command, int *infp, int *outfp, int *errfp) {

    int p_stdin[2], p_stdout[2], p_stderr[2];
    pid_t pid;

    if (pipe(p_stdin) != 0 || pipe(p_stdout) != 0 || pipe(p_stderr) != 0)
        return -1;

    pid = fork();

    if (pid < 0)
        return pid;
    else if (pid == 0)
    {
        close(p_stdin[WRITE]);
        dup2(p_stdin[READ], READ);
        close(p_stdout[READ]);
        dup2(p_stdout[WRITE], WRITE);

        execvp(*command, command);
        perror("execvp");
        exit(1);
    }

    if (infp == NULL)
        close(p_stdin[WRITE]);
    else
        *infp = p_stdin[WRITE];

    if (outfp == NULL)
        close(p_stdout[READ]);
    else
        *outfp = p_stdout[READ];
    
    if (errfp == NULL)
        close(p_stderr[READ]);
    else
        *outfp = p_stderr[READ];

    return pid;
}
#endif

#if defined (__WIN32)
#define CLIENT_RECIVES_ENTRY 0x0
#define CLIENT_RECIVES_EXIT 0x1
#define ASYNC_CLIENT 0x2
#define ASYNC_SERVER 0x4

PipeCombo* CreatePipeCombo(access_t* p_access,char flags){
    PipeCombo* combo=malloc(sizeof(PipeCombo));
    SECURITY_ATTRIBUTES secatr;
    secatr.nLength=sizeof(SECURITY_ATTRIBUTES);
    secatr.bInheritHandle=TRUE;
    secatr.lpSecurityDescriptor=NULL;
    char* pipeName=malloc(sizeof("\\\\.\\pipe\\")+20+1);
    sprintf(pipeName,"\\\\.\\pipe\\");
    char* rand=RandomString(20);
    strcat(pipeName,rand);
    free(rand);
    unsigned long dwOpenMode=PIPE_ACCESS_DUPLEX;
    dwOpenMode |= flags&ASYNC_SERVER ? FILE_FLAG_OVERLAPPED : 0;
    combo->pipe=CreateNamedPipeA(pipeName,dwOpenMode,PIPE_TYPE_BYTE|PIPE_READMODE_BYTE|PIPE_REJECT_REMOTE_CLIENTS,1,1024,1024,0,NULL);
    if(combo->pipe==INVALID_HANDLE_VALUE){
      //Handle error
      ErrMessage(p_access,"Creation of named pipe failed",GetLastError());
      free(combo);
      return NULL;
    }
    dwOpenMode=FILE_ATTRIBUTE_NORMAL;
    dwOpenMode |= flags&ASYNC_CLIENT ? FILE_FLAG_OVERLAPPED : 0;
    combo->entry=CreateFileA(pipeName,flags&CLIENT_RECIVES_EXIT ? GENERIC_READ : GENERIC_WRITE,0,&secatr,
                            OPEN_EXISTING,dwOpenMode,NULL);
    if(combo->entry==INVALID_HANDLE_VALUE){
      //Handle error
      ErrMessage(p_access,"Creation of slave pipe handler failed",GetLastError());
      free(combo);
      return NULL;
    }
    
    if(flags&CLIENT_RECIVES_EXIT){
      combo->exit=combo->entry;
      combo->entry=combo->pipe;
    }else{
      combo->exit=combo->pipe;
    }
      
    free(pipeName);
    return combo;
}

void FreePipeCombo(PipeCombo* combo){
    if(combo->entry!=combo->pipe) CloseHandle(combo->entry);
    if(combo->exit!=combo->pipe) CloseHandle(combo->exit);
    DisconnectNamedPipe(combo->pipe);
    CloseHandle(combo->pipe);
    free(combo);
}

Process* InvokeCommand(access_t* p_access,char* command,unsigned char asyncPipe){
    Process* process=malloc(sizeof(Process));
    STARTUPINFO si;
    memset(&si,0,sizeof(si));
    memset(&process->pi,0,sizeof(PROCESS_INFORMATION));
    process->output=CreatePipeCombo(p_access,asyncPipe ? CLIENT_RECIVES_ENTRY | ASYNC_SERVER : CLIENT_RECIVES_ENTRY);
    process->input=CreatePipeCombo(p_access,asyncPipe ? CLIENT_RECIVES_EXIT | ASYNC_SERVER : CLIENT_RECIVES_EXIT);
    process->error=CreatePipeCombo(p_access,asyncPipe ? CLIENT_RECIVES_ENTRY | ASYNC_SERVER : CLIENT_RECIVES_ENTRY);
    if(!process->input||!process->output||!process->error){
      msg_Dbg(p_access,command);
      free(process);
      return NULL;
    }
    msg_Dbg(p_access,"PIP");
    si.cb=sizeof(STARTUPINFO);
    si.dwFlags|=STARTF_USESTDHANDLES;
    si.hStdInput=process->input->exit;
    si.hStdError=process->error->entry;
    si.hStdOutput=process->output->entry;
    wchar_t* commandW=malloc(sizeof(wchar_t)*(strlen(command)+1));
    mbstowcs(commandW,command,sizeof(wchar_t)*(strlen(command)+1));
    if(!CreateProcess(NULL,(LPWSTR)commandW,NULL,NULL,TRUE,0,NULL,NULL,&si,&process->pi)){
      //Handle error
      msg_Dbg(p_access,command);
      free(commandW);
      FreeProcess(process);
      ErrMessage(p_access,"Command execution failed",GetLastError());
      return NULL;
    }
    return process;
}

void FreeProcess(Process* process){
    if(process->input) FreePipeCombo(process->input);
    if(process->output) FreePipeCombo(process->output);
    if(process->error) FreePipeCombo(process->error);
    CloseHandle(process->pi.hProcess);
    CloseHandle(process->pi.hThread);
    free(process);
}

#endif
/**
 * Starts our example interface.
 */
int Open(vlc_object_t *obj)
{
    access_t *p_access = (access_t *)obj;
    msg_Dbg(p_access,"Livestreamer module opened");
    if(strcmp(p_access->psz_access, "livestreamer")!=0){
      return VLC_EGENERIC;
    }
    char* command=malloc(sizeof(LIVESTREAMER_PATH)+sizeof(" \"http://")+strlen(p_access->psz_location)+sizeof("\""));
    sprintf(command,LIVESTREAMER_PATH);
    strcat(command," \"http://");
    strcat(command,p_access->psz_location);
    strcat(command,"\"");
#if defined (__posix__)
    FILE* pipe=popen(command,"r");
#elif defined (_WIN32)
    Process* prePro=InvokeCommand(p_access,command,FALSE);
    if(!prePro) return VLC_EGENERIC;
    FILE* pipe=_fdopen(_open_osfhandle((long)prePro->output->exit,O_RDONLY),"r");
#endif
    unsigned char found=0;
    msg_Dbg(p_access,"Malloc");
    char* line=malloc(sizeof(char)*150);
    while(found==0){
      msg_Dbg(p_access,"Ussles1");
      if(!fgets(line,sizeof(char)*150,pipe)){
        break;
      }
      msg_Dbg(p_access,line);
      if(strstr(line,"Available streams")){
        found=1; 
      }else if(strstr(line,"error")){
        found=0;
        break;
      }
      msg_Dbg(p_access,"Ussles2");
    }
#if defined (__posix__)
    pclose(pipe);
#elif defined (_WIN32)
    fclose(pipe);
#endif
    free(line);
    if(!found){
      msg_Dbg(p_access,"Stream not found");
      return VLC_EGENERIC;
    }
    msg_Dbg(p_access,"Stream found");
    //Open stream
    free(command);
    ModuleInstance* instance=malloc(sizeof(ModuleInstance));
#if defined(__posix__)
    command=malloc(sizeof("http://")+strlen(p_access->psz_location));
    sprintf(command,"http://");
    strcat(command,p_access->psz_location);
    const char* commandListed[]={LIVESTREAMER_PATH,command,"best","-O",NULL};
    int pipeNum;
    instance->livestreamerProcess=popen2(commandListed,NULL,&pipeNum,NULL);
    instance->livestreamerStdout=fdopen(pipeNum,"r");
#elif defined(_WIN32)
    command=malloc(sizeof(LIVESTREAMER_PATH)+sizeof(" \"http://")+strlen(p_access->psz_location)+sizeof("\" best -O")+1);
    sprintf(command,LIVESTREAMER_PATH);
    strcat(command," \"http://");
    strcat(command,p_access->psz_location);
    strcat(command,"\" best -O");
    instance->livestreamerProcess=InvokeCommand(p_access,command,TRUE);
    if(!instance->livestreamerProcess)return VLC_EGENERIC;
    memset(&instance->outputExitTransaction,0,sizeof(OVERLAPPED));
    instance->outputExitTransaction.hEvent=CreateEvent(NULL,TRUE,TRUE,NULL);
    if(instance->outputExitTransaction.hEvent==INVALID_HANDLE_VALUE){
      ErrMessage(p_access,"Async event creation failed",GetLastError());
    }
    unsigned long wR=WaitForSingleObject(instance->outputExitTransaction.hEvent,INFINITE);
    if(wR!=WAIT_OBJECT_0){
      ErrMessage(p_access,"Async event didn't get signaled",wR);
    }
    instance->buffer=0;
#endif
    p_access->p_sys=instance;
    p_access->pf_seek=NULL;
#if defined (__posix__)
    p_access->pf_read=&Read;
    p_access->pf_block=NULL;
#elif defined (_WIN32)
    p_access->pf_read=NULL;
    p_access->pf_block=&Block;
#endif
    p_access->pf_control=&Control;
    free(command);
    return VLC_SUCCESS;
}

#if defined (__posix__)
ssize_t Read(access_t* p_access,uint8_t* buffer,size_t sz){
    ModuleInstance* instance=(ModuleInstance*)p_access->p_sys;
    unsigned long size;
    size=(unsigned long)fread(buffer,1,sz,instance->livestreamerStdout);
    if(size==0 && !feof(instance->livestreamerStdout)) return -1;
    if(ferror(instance->livestreamerStdout)){
      msg_Err(p_access,"Error on reading from pipe");
    }
    return (ssize_t)size;
}
#elif defined (_WIN32)

block_t* Block(access_t* p_access){
    //msg_Err(p_access,"Block I");
    ModuleInstance* instance=(ModuleInstance*)p_access->p_sys;
    if(!instance->buffer) instance->buffer=malloc(1024);
    unsigned long wR=WaitForSingleObject(instance->outputExitTransaction.hEvent,100);
    switch(wR){
      case WAIT_ABANDONED:
        ErrMessage(p_access,"Async event got abandoned",GetLastError());
        p_access->info.b_eof=TRUE;
        return NULL;
      case WAIT_TIMEOUT:
        return NULL;
      case WAIT_OBJECT_0:
        break;
      default:
        ErrMessage(p_access,"Async event failed",GetLastError());
        p_access->info.b_eof=TRUE;
        return NULL;
    }
    unsigned long size;
    if(!GetOverlappedResult(instance->livestreamerProcess->output->exit,&instance->outputExitTransaction,&size,FALSE))
    {
      if(GetLastError()==ERROR_IO_INCOMPLETE){
        //msg_Err(p_access,"Block P");
        return NULL;
      }
      if(GetLastError()==ERROR_HANDLE_EOF){
        p_access->info.b_eof=TRUE;
        //msg_Err(p_access,"Block EOF");
        return NULL;
      }
      ErrMessage(p_access,"Reading request of pipe failed",GetLastError());
      //p_access->info.b_eof=TRUE;
      return NULL;
    }
    block_t* rt=block_Alloc(size);
    memcpy(rt->p_buffer,instance->buffer,size);
    p_access->info.i_pos += size;
    if(!ReadFile(instance->livestreamerProcess->output->exit,(LPVOID)instance->buffer,1024,NULL,&instance->outputExitTransaction))
    {
      if(GetLastError()==ERROR_IO_PENDING){
        //msg_Err(p_access,"Block P2");
        block_Release(rt);
        return NULL;
      }
      if(GetLastError()==ERROR_HANDLE_EOF){
        p_access->info.b_eof=TRUE;
        //msg_Err(p_access,"Block EOF2");
        block_Release(rt);
        return NULL;
      }
      ErrMessage(p_access,"Reading request of pipe failed2",GetLastError());
      p_access->info.b_eof=TRUE;
      return NULL;
    }
    //ErrMessage(p_access,"Block E",size);
    return rt;
}
#endif

int Control(access_t *p_access, int i_query, va_list args){
  bool    *pb_bool;
  int64_t *pi_64;
  switch(i_query){
    case ACCESS_CAN_SEEK:
      pb_bool = (bool*)va_arg( args, bool* );
      *pb_bool = 0;
      break;
    case ACCESS_CAN_FASTSEEK:
      pb_bool = (bool*)va_arg( args, bool* );
      *pb_bool = 0;
      break;
    case ACCESS_CAN_PAUSE:
      pb_bool = (bool*)va_arg( args, bool* );
      *pb_bool = 0;
      break;
    case ACCESS_CAN_CONTROL_PACE:
      pb_bool = (bool*)va_arg( args, bool* );
      *pb_bool = 0;
      break;
    case ACCESS_GET_PTS_DELAY:
      pi_64 = (int64_t*)va_arg( args, int64_t * );
      *pi_64 = 5000;
      break;
    case ACCESS_SET_PAUSE_STATE:
      break;
    default:
      return VLC_EGENERIC;
  }
  return VLC_SUCCESS;
}

/**
 * Stops the interface. 
 */
void Close(vlc_object_t *obj)
{
    access_t *p_access = (access_t *)obj;
    ModuleInstance* instance=(ModuleInstance*)p_access->p_sys;
#if defined (__posix__)
    kill(instance->livestreamerProcess,SIGKILL);
    waitpid(instance->livestreamerProcess, NULL, 0);
    fclose(instance->livestreamerStdout);
#elif defined (_WIN32)
    TerminateProcess(instance->livestreamerProcess->pi.hProcess,0);
    FreeProcess(instance->livestreamerProcess);
    CloseHandle(instance->outputExitTransaction.hEvent);
    if(instance->buffer) free(instance->buffer);
#endif
    /* Free internal state */
    free(instance);
}
